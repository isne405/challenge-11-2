#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::headPush(int x){
	Node *tmp = new Node(x);
	if (head == 0){
		head = tmp;
		tail = tmp;
	}
	else{
		tmp->next = head;
		head->prev = tmp;
		head = tmp;
	}
}

void List::tailPush(int num ){
	Node *tmp = new Node(num);
	if (head == 0){
		head = tmp;
		tail = tmp;
	}
	else {
		tmp->prev = tail;
		tail->next = tmp;
		tail = tmp;
	}
}


int List::headPop(){
	Node *tmp = head;
	int x;
	if (head != tail){
		Node *tmp2 = head;
		tmp = head->next;
		x = head->info;
		head = tmp;
		delete tmp2;
	}
	else{
		x = head->info;
		delete head;
	}
	return x;
}

int List::tailPop(){
	Node* tmp = tail;
	int TMP;
	if (head != tail){
		TMP = tail->info;
		tail = tail->prev;
		delete tmp;
	}
	else{
		TMP = tail->info;
		delete tail;
	}
	return TMP;
}

void List::deleteNode(int nod){
	Node *tmp = head;
	Node *Delete;
	int find = 0;
	if (head->info == nod){
		head = head->next;
		delete tmp;
	}
	else if (tail->info == nod){
		tmp = tail;
		tail = tail->prev;
		tail->next = 0;
		delete tmp;
	}
	else{
		while (tmp->next->info!=nod){
			cout << "x";
			if (tmp->next == 0){
				cout << "That number don't be there";
				break;
			}
			if (tmp->next->info != nod)
				tmp = tmp->next;
		}
		if (tmp->next->info == nod){
			cout << "Deleted";
			Node *tmp2 = tmp;
			Node *tmp3 = tmp;
			tmp2 = tmp->next->next;
			tmp3 = tmp->next;
			tmp->next = tmp3;
			tmp3->prev = tmp;
			delete tmp2;
		}
	}
}


bool List::isInList(int num){
	Node *tmp = head;

	while (tmp->info != num){
		tmp = tmp->next;
		if (tmp == tail && tmp->info != num){
			return false;
		}
		else{
			return true;
		}
	}
	return true;
}


